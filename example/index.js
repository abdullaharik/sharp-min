/* jshint node: true */
'use strict';

const config = require('./config');

// BuiltIn Modules
const http = require('http');
const path = require('path');

const resizer = require('../');

const server = (req, res) => {
  try {
    const { url } = req;
    resizer(
      {
        write: false,
        // return: false,
        path: path.join(path.dirname(require.main.filename), 'public/images')
      },
      url
    ).pipe(res);
  } catch (err) {
    res.send(err.message).status(500);
  }
};

http.createServer(server).listen(config.server.PORT, config.server.IP);

process.on('SIGTERM', () => {
  console.info('SIGTERM signal received.');
  console.log('Closing http server.');
  server.close(() => {
    console.log('Http server closed.');
    // boolean means [force], see in mongoose doc
    mongoose.connection.close(false, () => {
      console.log('MongoDb connection closed.');
      process.exit(0);
    });
  });
});
