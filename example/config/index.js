require('dotenv').config();
const path = require('path');

const config = {
  server: {
    IP: process.env.IP || '127.0.0.1',
    PORT: process.env.PORT || 3334
  }
  // image: {
  //   path: path.join(path.dirname(require.main.filename), 'public/images'),
  //   type: {
  //     jpeg: { quality: 50 },
  //     png: {},
  //     webp: {}
  //   },
  //   webp: true
  //   // set: ['201x201', '301x301']
  // }
};

module.exports = config;
