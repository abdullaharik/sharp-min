// (function() {

// 'use strict';

const fs = require('fs');
const path = require('path');
const Sharp = require('sharp');

/**
 * Regular Expression Catch's
 *
 * /200x200/filename.jpg # basic with defaults
 * /200x200/filename.jpg.webp # make webp
 * /w200/filename.jpg # resize by width
 * /h200/filename.jpg # resize by height
 * /200x200a1/filename.jpg #not active
 * /200x200q1/filename.jpg #quality for jpeg
 */
const regex = /\/((?:(?:(\d+)x(\d+))|(?:w(\d+))|(?:h(\d+)))(?:(?:q(\d+))?(?:a(\d+))?))\/(.*(gif|jpg|jpeg|tiff|png))(?:\.(webp))?/;
const getIntOrNull = x => parseInt(x, 10) || null;

const getOptions = url => {
  const match = url.match(regex);
  // FULL SET: {full,folder,width,height,onlyWidth,onlyHeight,quality,action,fileName,fileExtension,isWebp};
  const [full, folder, , , , , , , fileName, ,] = match;
  let [
    ,
    ,
    width,
    height,
    onlyWidth,
    onlyHeight,
    quality,
    action,
    ,
    fileExtension,
    isWebp
  ] = match;

  width = getIntOrNull(width);
  height = getIntOrNull(height);
  onlyWidth = getIntOrNull(onlyWidth);
  onlyHeight = getIntOrNull(onlyHeight);
  quality = getIntOrNull(quality);
  action = getIntOrNull(action);
  fileExtension = fileExtension ? fileExtension.toLowerCase() : fileExtension;
  isWebp = isWebp ? isWebp.toLowerCase() : isWebp;

  return {
    full,
    folder,
    width,
    height,
    onlyWidth,
    onlyHeight,
    quality,
    action,
    fileName,
    fileExtension,
    isWebp
  };
};

const getTransformer = ({
  width,
  height,
  onlyWidth,
  onlyHeight,
  fileExtension,
  quality,
  isWebp
}) => {
  const sharp = new Sharp();

  if (width && height) {
    sharp.resize(width, height);
  } else if (onlyWidth) {
    sharp.resize(onlyWidth);
  } else if (onlyHeight) {
    sharp.resize(null, onlyHeight);
  }

  if (fileExtension === 'jpg' || fileExtension === 'jpeg') {
    sharp.jpeg({ quality });
  }

  if (isWebp) {
    sharp.webp({ quality });
  }

  return sharp;
};

/**
 * Path is a core module
 * TODO:IO monad kullanılarak tekrar yazılacak
 * @param {object} fs
 * @param {object} path
 * @param {string} targetPath
 */

const createFullPath = targetPath => {
  // https://stackoverflow.com/questions/31645738/how-to-create-full-path-with-nodes-fs-mkdirsync
  const { sep } = path;
  const initDir = path.isAbsolute(targetPath) ? sep : '';
  targetPath.split(sep).reduce((parentDir, childDir) => {
    const curDir = path.resolve(parentDir, childDir);
    if (!fs.existsSync(curDir)) {
      fs.mkdirSync(curDir);
    }
    return curDir;
  }, initDir);

  return targetPath;
};

const resize = (config, url) => {
  const options = getOptions(url);
  const transform = getTransformer(options);

  const origin = path.join(config.path, options.fileName);
  const target = path.join(config.path, options.folder, options.fileName);

  const wstream = fs.createWriteStream(target);
  const rstream = fs.createReadStream(origin);

  wstream.once('error', error => {
    if (error.code === 'ENOENT') {
      createFullPath(path.dirname(target));
      resize(config, url);
    }
  });

  rstream.pipe(transform);

  if (config.write !== undefined || config.write !== false) {
    transform.pipe(wstream);
  }

  if (config.return !== undefined || config.return !== false) {
    return transform;
  }

  return true;
};

module.exports = resize;
// console.log('test');
// })();
