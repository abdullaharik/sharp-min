# sharp-min

sharp-min is a stream based middleware for Sharp image resizer

### Example URL format

```
 * /200x200/filename.jpg # basic with defaults
 * /200x200/filename.jpg.webp # make webp
 * /w200/filename.jpg # resize by width
 * /h200/filename.jpg # resize by height
 * /200x200a1/filename.jpg #not active
 * /200x200q1/filename.jpg #quality for jpeg

```

### Example Server Conf

```
const config = require('./config');

// BuiltIn Modules
const http = require('http');
const path = require('path');

const resizer = require('../');

const server = (req, res) => {
  try {
    const { url } = req;
    resizer(
      {
        write: false,
        // return: false,
        path: path.join(path.dirname(require.main.filename), 'public/images')
      },
      url
    ).pipe(res);
  } catch (err) {
    res.send(err.message).status(500);
  }
};

http.createServer(server).listen(config.server.PORT, config.server.IP);

```

## Built With

* [Sharp](https://github.com/lovell/sharp) - High performance Node.js image processing, the fastest module to resize JPEG, PNG, WebP and TIFF images. Uses the libvips library.

## Authors

* **Abdullah Arık** - *Initial work* - [abdullaharik](http://abdullaharik.com)


## License

This project is licensed under the MIT License
